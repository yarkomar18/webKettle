/*
Name:    wifiKettle.ino
Created: 01.02.2018 22:51:22
Author:  Yaroslav
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//   -- ПОРТЫ --
// радиомодуль
RF24 radio(2, 15);    // Set up nRF24L01 radio on SPI bus plus pins 2 for CE and 15 for CSN
            // LCD дисплей
LiquidCrystal_I2C lcd(0x3F, 16, 2);
// коды кнопок
int buttons = A0;
int relay = 16;
// -------------------------

// -- ПЕРЕМЕННЫЕ --
// номер меню
int menu = 0;
// режим термостата
bool termoMode = 0;
// максимальная температура
int maxTemperature = 100;
// заданная тепемпература для нагрева
int setTemperature = 100;
// текущая считанная температура
int currentTemperature = 0;
// предыдущая считанная температура
int prevTemperature = 0;
// состояние воды 
bool heating = 0;

// wifi к которому будем подключаться
String wifiName = "noname";
String wifiPass = "nopass";

bool isConnected = false;
const char *ssid = "SmartKettle";

ESP8266WebServer server(80);

// -------------------------

void setup()
{
  Serial.begin(115200);
  Serial.println("\nstart");
  // настраиваем wifi
  WiFi.softAP(ssid);
  server.on("/wifiSetup", HTTP_POST, handleWifiSetup); // настраиваем wifi
  server.on("/setpower", HTTP_POST, handleSetPower); // включаем нагрев
  server.on("/getpower", HTTP_GET, handleGetPower); // узнаем статус вкл или выкл
  server.on("/post_t_temperature", HTTP_POST, handlePostTargetTemp); // устанавливаем нужную температуру
  server.on("/get_t_temperature", HTTP_GET, handleGetTargetTemp); // узнаем установленную температуру
  server.on("/get_c_temperature", HTTP_GET, handleGetCurrentTemp); // получаем текущую температуру
  server.on("/post_termomode", HTTP_POST, handlePostTermoMode); // устанавливаем режим термостата
  server.on("/get_termomode", HTTP_GET, handleGetTermoMode); // узнаем статус термостата
  server.on("/isHeated", HTTP_GET, handleisHeated); // узнаем нагрета ли вода
  server.begin();

  // включаем радиомодуль
  //  SPI.begin();
  //  SPI.setDataMode(SPI_MODE0);
  //  SPI.setBitOrder(MSBFIRST);   
  radio.begin();                                        // Инициируем работу nRF24L01+
  radio.setChannel(10);                                  // Указываем канал приёма данных (от 0 до 127), 5 - значит приём данных осуществляется на частоте 2,405 ГГц (на одном канале может быть только 1 приёмник и до 6 передатчиков)
  radio.setPayloadSize(2);
  radio.setCRCLength(RF24_CRC_16);
  radio.setDataRate(RF24_1MBPS);                   // Указываем скорость передачи данных (RF24_250KBPS, RF24_1MBPS, RF24_2MBPS), RF24_1MBPS - 1Мбит/сек
  radio.setPALevel(RF24_PA_MAX);                 // Указываем мощность передатчика (RF24_PA_MIN=-18dBm, RF24_PA_LOW=-12dBm, RF24_PA_HIGH=-6dBm, RF24_PA_MAX=0dBm)
  radio.openReadingPipe(1, 0xc2c2c2c2c2LL);            // Открываем 1 трубу с идентификатором 0x1234567890 для приема данных (на одном канале может быть открыто до 6 разных труб, которые должны отличаться только последним байтом идентификатора)
  radio.startListening();                             // Включаем приемник, начинаем прослушивать открытую трубу
  radio.printDetails();

  pinMode(buttons, INPUT);
  pinMode(relay, OUTPUT);
  digitalWrite(relay, LOW);

  // включаем дисплей
  lcd.init();
  lcd.backlight();

  // открываем меню 0
 updateMenu0();    
}

void loop() {
    server.handleClient();

}



void connectWiFi() {
  for(int i = 0; i < 100; i++){
    if (WiFi.status() != WL_CONNECTED){
      delay(100);  
      WiFi.begin(wifiName.c_str(), wifiPass.c_str());
      // Begin WiFi connection procedure
      Serial.print("\nConnecting to ");
      Serial.print(wifiName.c_str());
      Serial.print(" access point");
    }
  
    if (WiFi.status() == WL_CONNECTED)
    {
      // Print result
      isConnected = true;
      Serial.print("Connected, IP address: ");
      Serial.println(WiFi.localIP());
      WiFi.mode(WIFI_OFF);
    }
  }
}



//---------------------------------------------------------------------

// настройка wifi
void handleWifiSetup() {
  if (server.hasArg("name") == false || server.hasArg("pass") == false) {
    server.send(400, "text/plain", "Body not received");
    return;
  }
  wifiName = server.arg("name");
  wifiPass = server.arg("pass");

  Serial.print("wifiName: ");
  Serial.println(wifiName);

  Serial.print("wifiPass: ");
  Serial.println(wifiPass);

  server.send(200, "text/plain", "OK");
  
  connectWiFi();
}





int getTemperature(){
  if(radio.available()){                                // Если в буфере имеются принятые данные
  radio.read(&currentTemperature, 2);                  // Читаем данные в массив data и указываем сколько байт читать     
  Serial.println(currentTemperature);
  }
  return currentTemperature;   // возвращает предыдущую температуту если не считало новую 
}

void printText(int _cPos, int _sPos, String _text) {
  lcd.setCursor(_cPos, _sPos);          
  lcd.print(_text);            
}

void updateMenu0() {
  Serial.print("menu 0\n");

  heating = 0;
  termoMode = 0;

  lcd.clear();
  String setTemperature_string = (String)setTemperature;
  printText(0, 0, "set temperature:");
  printText(0, 1, setTemperature_string);
  printText(setTemperature_string.length() + 1, 1, "C");
}

void updateMenu1() {
  Serial.print("menu 1\n");
  lcd.clear();

  String setTemperature_string = (String)setTemperature;
  printText(0, 0, "required");
  printText(9, 0, setTemperature_string);
  printText(9 + setTemperature_string.length() + 1, 0, "C");


  String current_temperature_string = (String)currentTemperature;
  printText(0, 1, "current");
  printText(8, 1, current_temperature_string);
  printText(8 + current_temperature_string.length() + 1, 1, "C");
}

// вкл\выкл
void handleSetPower() {
  if (server.hasArg("plain") == false) {
    server.send(400, "text/plain", "Body not received");
    return;
  }
  String body = server.arg("plain");
  menu = body.toInt();

  Serial.print("power ");
  Serial.println(menu);
  if (menu == 1) {
    server.send(200, "text/plain", "OK");
  }
  if (menu == 0) {
    server.send(200, "text/plain", "OK");
  }
}

// узнать вкл\выкл
void handleGetPower() {
  Serial.print("power ");
  Serial.println(menu);

  server.send(200, "text/plain", String(menu).c_str());
}

// установка температуры
void handlePostTargetTemp() {
  if (server.hasArg("plain") == false) {
    server.send(400, "text/plain", "Body not received");
    return;
  }
  String body = server.arg("plain");
  setTemperature = body.toInt();

  Serial.print(setTemperature);
  Serial.println(setTemperature);

  server.send(200, "text/plain", "OK");
}

// узнать установленную температуру
void handleGetTargetTemp() {
  Serial.print("getrequiredTemperature");
  Serial.println(setTemperature);

  server.send(200, "text/plain", String(setTemperature).c_str());
}

// узнать текущую температуру
void handleGetCurrentTemp() {
  Serial.print("СurrentTemperature: ");
  Serial.println(currentTemperature);

  server.send(200, "text/plain", String(currentTemperature).c_str());
}

// установка режима термостата
void handlePostTermoMode() {
  if (server.hasArg("plain") == false) {
    server.send(400, "text/plain", "Body not received");
    return;
  }
  String body = server.arg("plain");
  termoMode = body.toInt();

  Serial.print("termoMode: ");
  Serial.println(termoMode);

  server.send(200, "text/plain", "OK");
}

// узнать режим термостата
void handleGetTermoMode() {
  Serial.print("СurrentTermoMode: ");
  Serial.println(termoMode);

  server.send(200, "text/plain", String(termoMode).c_str());
}

// узнать состояние нагрева воды
void handleisHeated() {
  Serial.print("WaterStatus: ");
  Serial.println(heating);

  server.send(200, "text/plain", String(heating).c_str());
}

