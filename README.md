# Smart socket
Socket witch can connecting to other devises by wifi or radio it smart home system. Can connect devise in other progect (Wireless thermometer).

- Attiny 13a
http://ww1.microchip.com/downloads/en/DeviceDoc/doc8126.pdf

- DS18b20
https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf

- NRF24
https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf

# Circuit board
https://lh3.googleusercontent.com/bp3z1IhuntkNipjVNYzOuQr8--dFnCn1ooLGRlfVLL81mMSl0tJf_j1lpaB6J7i3j3UFDLa5wTx7

https://lh3.googleusercontent.com/_2jSjjbZnte3N4lhyuwJDAOASajeMPZBBqV1T5gICKgxrV66sNJgmrBcvQs0eEphHDyJxdwY5Lkw

https://lh3.googleusercontent.com/kbMlnSwoPs0Xs7uTrMIQZw7V321Bexjvf3X6JRD5ldrx8hjif_EVRFdAFSE-owUISAm6s8FgUtIo

https://lh3.googleusercontent.com/neQnHUT4aHJVg40ldL1oNPYq7Uy2A8i80TCQ1EadDnxgsz2jOGsONimvW4cfo3hsJfEbM2bcOFwS

# 3D model
https://lh3.googleusercontent.com/HmVltVnPCa67-GXrDqYlSFX88rLj2q7eGzWKnj1jhK0UzlKlJEc6M19lmtDsUNnSPJ9M5eCX9UDu

https://lh3.googleusercontent.com/4wHYXmbjXzADD3yAB9JX3D_nEw6ADY-HOWSMLeg6OkGcBUPe40OIp3pAZ5vj4Fq7Kh2gPSxSFpbZ
